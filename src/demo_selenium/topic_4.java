package demo_selenium;

import org.testng.annotations.Test;
/*import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;*/
import org.testng.annotations.BeforeTest;
/*
import static org.junit.Assert.assertEquals;*/

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class topic_4 {
	WebDriver driver;
	@BeforeTest
	public void int_data()
	{
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		
		
	}

@Test
	
	public void TestCase01_dropdownList() throws Exception {
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize(); 
		
		String homePageTittle = driver.getTitle();
		Assert.assertEquals("SELENIUM WEBDRIVER FORM DEMO", homePageTittle);

		Select dropdown = new Select(driver.findElement(By.xpath("//select[@name='user_job1']")));
		
		Assert.assertFalse(dropdown.isMultiple());
		
		dropdown.selectByVisibleText("Automation Tester");
		Assert.assertEquals("Automation Tester", dropdown.getFirstSelectedOption().getText());
		Thread.sleep(5000);
		
		dropdown.selectByValue("manual");
		Assert.assertEquals("Manual Tester", dropdown.getFirstSelectedOption().getText());
		Thread.sleep(4000);
		
		dropdown.selectByIndex(3);
		Assert.assertEquals("Mobile Tester", dropdown.getFirstSelectedOption().getText() );
		
		Thread.sleep(6000);
		
		Assert.assertEquals(5, dropdown.getOptions().size());
		
	}
	
	@Test
	public void Testcase02_TextBox()
	{
		 String ipname, ipGender, ipdayofbirth, ipaddres, ipspace, ipstate,
		 ippin, ipmobile, ipemail, ippas, cusName, cusAdress;
		 
		 ipname ="//input[@name='name']";
		 ipGender ="//input[@value='m']";
		 ipdayofbirth ="//input[@name='dob']";
		 ipaddres ="//textarea[@name='addr']";
		 ipspace ="//input[@name='city']";
		 ipstate ="//input[@name='state']";
		 ippin ="//input[@name='pinno']";
		 ipmobile ="//input[@name='telephoneno']";
		 ipemail ="//input[@name='emailid']";
		 ippas="//input[@name='password']"; 
		 
		 cusName = "La Thi Thanh";
		 cusAdress = "a 75 bacbhj dang tan binh";
		
		driver.get("http://demo.guru99.com/v4");
		driver.manage().timeouts().implicitlyWait(10,  TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		WebElement UserIDlogin = driver.findElement(By.xpath("//input[@name='uid']"));
		UserIDlogin.sendKeys("mngr127509");
		
		WebElement password = driver.findElement(By.xpath("//input[@name='password']"));
		password.sendKeys("apYdagY");
		
		WebElement btnLogin = driver.findElement(By.xpath("//input[@name='btnLogin']"));
		btnLogin.click();
		
		WebElement newcustomer = driver.findElement(By.xpath("//a[text()='New Customer']"));
		newcustomer.click();
		
		
		WebElement customerName = driver.findElement(By.xpath(ipname));
		customerName.sendKeys(cusName);
		
		WebElement Gender = driver.findElement(By.xpath(ipGender));
		Gender.click();
		
		WebElement DayOfBirth = driver.findElement(By.xpath(ipdayofbirth));
		DayOfBirth.sendKeys("10/11/1998");
		WebElement Address = driver.findElement(By.xpath(ipaddres));
		Address.sendKeys(cusAdress);
		WebElement City = driver.findElement(By.xpath(ipspace));
		City.sendKeys("Ho Chi Minh");
		WebElement State = driver.findElement(By.xpath(ipstate));
		State.sendKeys("tan binh");
		WebElement pin = driver.findElement(By.xpath(ippin));
		pin.sendKeys("123456789");
		WebElement MobileNuber = driver.findElement(By.xpath(ipmobile));
		MobileNuber.sendKeys("0933828391");
		
		Random ganarate = new Random();
		WebElement Email = driver.findElement(By.xpath(ipemail));
		Email.sendKeys("lamanaka" +ganarate.nextInt(1000)+ "@gmail.com" );
		
		
		WebElement Pas = driver.findElement(By.xpath(ippas));	
		Pas.sendKeys("123@name");
		WebElement BtnSubmit = driver.findElement(By.xpath("//input[@name='sub']"));
		BtnSubmit.click();
		
		 String customeId;
		 customeId = driver.findElement(By.xpath("//td[contains(text(),'Customer ID')]/following-sibling::td")).getText();
		System.out.println(customeId);
		
		WebElement editCustomer = driver.findElement(By.xpath("//a[text()='Edit Customer']"));
		editCustomer.click();
		
		WebElement customerIdget = driver.findElement(By.xpath("//input[@name='cusid']"));
		customerIdget.sendKeys(customeId);
		
		WebElement accCustomeSubmit = driver.findElement(By.xpath("//input[@name='AccSubmit']"));
		accCustomeSubmit.click();
		
		//verify
		Assert.assertEquals(cusName, driver.findElement(By.xpath("//input[@name='name']"))
				.getAttribute("value"));
		
		Assert.assertEquals(cusAdress, driver.findElement(By.xpath("//textarea[@name='addr']"))
				.getText());
		
		
		
		driver.findElement(By.xpath("//textarea[@name='addr']")).clear();
		
		driver.findElement(By.xpath("//textarea[@name='addr']")).sendKeys("dia chi moi");
		
		driver.findElement(By.xpath("//input[@name='sub']")).click();
		
		
		
	}
	

	@AfterClass
	public void afterClass() {

		driver.quit();
	}
}
