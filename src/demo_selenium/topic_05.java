package demo_selenium;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class topic_05 {
	WebDriver driver;
	@BeforeTest
	public void int_data()
	{
		/*System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		driver = new ChromeDriver();*/
		
		driver = new FirefoxDriver();
		
		
	}

@Test
	public void TestCase01_button() {

		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize(); 
		
		String homePageTittle = driver.getTitle();
		Assert.assertEquals("SELENIUM WEBDRIVER FORM DEMO", homePageTittle);
		
		WebElement enableButton_01 = driver.findElement(By.xpath("//button[@id='button-enabled']"));
		enableButton_01.click();
		driver.navigate().back();
		
		WebElement enableButton_02 = driver.findElement(By.xpath("//button[@id='button-enabled']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", enableButton_02);
		
	}
	@Test
	public void TestCase02_checkbox() throws InterruptedException {

		driver.get("http://demos.telerik.com/kendo-ui/styling/checkboxes");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize(); 
		
		String homePageTittle = driver.getTitle();
		Assert.assertEquals("jQuery icons styling example | Kendo UI Web demos", homePageTittle);
		
		WebElement RadioButton = driver.findElement(By.xpath("//label[contains(text(),'Dual-zone air conditioning')]/preceding-sibling::input"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", RadioButton);
		
		Thread.sleep(3000);
		//Assert.assertTrue(RadioButton.isSelected());
		
		WebElement RadioButton_02 = driver.findElement(By.xpath("//label[contains(text(),'Dual-zone air conditioning')]/preceding-sibling::input"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click()", RadioButton_02);
		
		Thread.sleep(3000);
		
		if(!RadioButton_02.isSelected())
		{
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", RadioButton_02);
			
		}
		
		
	}
	
	@Test
	
	public void TestCase03_RadioButton() throws InterruptedException {

		driver.get("http://demos.telerik.com/kendo-ui/styling/radios");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//driver.manage().window().maximize(); 
		
		
		WebElement RadioButton_02 = driver.findElement(By.xpath("//label[contains(text(),'2.0 Petrol, 147kW')]/preceding-sibling::input"));
		((JavascriptExecutor) driver).executeScript("arguments[0].click();", RadioButton_02);
		
		Thread.sleep(3000);
		
		if(!RadioButton_02.isSelected())
		{
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", RadioButton_02);
			
		}
		Thread.sleep(3000);
		
		
	}
	
	
	@Test
	public void TestCase04_buttonAlert() throws Exception {

		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize(); 
		
		String homePageTittle = driver.getTitle();
		Assert.assertEquals("SELENIUM WEBDRIVER FORM DEMO", homePageTittle);
		
		WebElement buttonAlert = driver.findElement(By.xpath("//button[contains(text(), 'Click for JS Alert')]"));
		buttonAlert.click();
		//Thread.sleep(3000);
		
		Alert alert= driver.switchTo().alert();
		Assert.assertEquals("I am a JS Alert", alert.getText());
		alert.accept();
		//Thread.sleep(3000);
		
		Assert.assertEquals("You clicked an alert successfully", driver.findElement(By.xpath("//p[@id='result']")).getText());
		
	}
	
	@Test
	public void TestCase04_buttonConfirm() throws Exception {

		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize(); 
		
		String homePageTittle = driver.getTitle();
		Assert.assertEquals("SELENIUM WEBDRIVER FORM DEMO", homePageTittle);
		
		WebElement buttonConfirm = driver.findElement(By.xpath("//button[contains(text(), 'Click for JS Confirm')]"));
		buttonConfirm.click();
		
		Thread.sleep(3000);
		
		Alert alert= driver.switchTo().alert();
		Assert.assertEquals("I am a JS Confirm", alert.getText());
		alert.dismiss();
		Thread.sleep(3000);
		
		Assert.assertEquals("You clicked: Cancel", 
				driver.findElement(By.xpath("//p[@id='result']")).getText());
		
	}
	
	@Test
	public void TestCase04_buttonPrompt() throws Exception {
		String name = "xin chao nhe";

		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize(); 
		
		String homePageTittle = driver.getTitle();
		Assert.assertEquals("SELENIUM WEBDRIVER FORM DEMO", homePageTittle);
		
		WebElement buttonPrompt = driver.findElement(By.xpath("//button[contains(text(), 'Click for JS Prompt')]"));
		buttonPrompt.click();
		
		Thread.sleep(3000);
		
		Alert alert= driver.switchTo().alert();
		Assert.assertEquals("I am a JS prompt", alert.getText());
		alert.sendKeys(name);
		alert.accept();
		Thread.sleep(3000);
		
		Assert.assertTrue(driver.findElement(By.xpath("//p[@id='result' and contains(text(),'" + name + "')]")).isDisplayed());
		
	}
	
	
	
	
	
	@AfterClass
	public void afterClass() {

		driver.quit();
	}
}
