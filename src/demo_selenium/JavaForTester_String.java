package demo_selenium;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class JavaForTester_String {
	WebDriver driver;
	String chuoi = "Automation Testing Tutorials Online 03";
	@BeforeTest
	public void int_data()
	{
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		
		
	}

	
	@Test
	public void TestCase01_() {
		int length = chuoi.length();
		char kytua = 'a';
		int demchuoi =0;
		for(int i=0;i<length;i++)
		{
		if(chuoi.charAt(i) ==	kytua)
			demchuoi ++;
		}
		System.out.println("so ky tu a la:" + demchuoi);
		//câu 2
		
		String kytu = "Testing";
		if(chuoi.contains(kytu)== true)
		System.out.println("co chuoi Testing");
		else
			System.out.println("khong co chuoi Testing");
		
		
		 // câu 3
		if(chuoi.startsWith("Automation"))
			System.out.println("chuoi bat dau bang Automation");
		else 
			System.out.println("chuoi khong bat dau bang Automation");
		
		 // câu 4
		if(chuoi.endsWith("Online"))
			System.out.println("chuoi ket thuc bang Online");
		else 
			System.out.println("chuoi khong ket thuc bang Online");
		
		//câu 5
		
				int vitrichuoi = chuoi.indexOf("Tutorials");
				System.out.println("vi tri cua Tutorials la "+vitrichuoi );
				
		//  câu 6
				System.out.println("chuoi sau thay the la : "+ chuoi.replaceAll("Online", "Offline"));
		
		//câu 7
		int demso =0;
		for(int i=0;i<length;i++)	
		{
		if(Character.isDigit(chuoi.charAt(i)))
			demso ++;
		}
		
		System.out.println("co "+ demso +"ky tu la ky tu so");
		
		
	}
	
	
	
	
	
	@AfterClass
	public void afterClass() {

		driver.quit();
	}
}
