package demo_selenium;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import java.sql.Driver;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class topic_07 {
	WebDriver driver;
	@BeforeTest
	public void int_data()
	{
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		
		
	}

	

	@Test
	public void TestCase01_iframe1() {
		driver.get("http://www.hdfcbank.com/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize(); 
		
		WebElement lookingForIframe = driver.findElement(By.xpath("//div[@class='flipBannerWrap']//iframe"));
		driver.switchTo().frame(lookingForIframe);
		
		//driver.switchTo().defaultContent();
		
		WebElement lookingforIframeText = driver.findElement(By.xpath("//span[@class='message-text']"));
		
		System.out.println("in ra chu: " + lookingforIframeText.getText());
		//Assert.assertEquals("What are you looking for?", lookingforIframeText.getText());
		
		driver.switchTo().defaultContent();
		
		WebElement ImageIframe = driver.findElement(By.xpath("//div[@class='slidingbanners']//iframe"));
		driver.switchTo().frame(ImageIframe);
		
		List <WebElement> imgeIframeSize = driver.findElements(By.xpath("//img[@class='bannerimage']"));
		int sizeimage = imgeIframeSize.size();
		
		Assert.assertEquals(6, sizeimage);
		
		for (WebElement image:imgeIframeSize)
		{
			image.isDisplayed();
			
		}
		
		System.out.println("so image co la: "+ sizeimage);
		driver.switchTo().defaultContent();
		
		WebElement FlipbannerIframe = driver.findElement(By.xpath("//div[@class='flipBanner']"));
		Assert.assertTrue(FlipbannerIframe.isDisplayed());
		
		
	}
	
	
	@Test
	public void TestCase02_iframe1() {
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize(); 
		
		String parentID = driver.getWindowHandle();
		WebElement clickhere = driver.findElement(By.xpath("//a[contains(text(), 'Click Here')]"));
		clickhere.click();
		
		switchToChildwindow(parentID);
		switchToWindowByTitle("Google");
		String googleTitle = driver.getTitle();
		Assert.assertEquals("Google", googleTitle);
		
		CloseAllWindowOutparentWindow(parentID);
		
		
	}
	

	@Test
	public void TestCase03_iframe1() {
		driver.get("https://www.hdfcbank.com/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize(); 
		
		/*if(driver.findElement(By.xpath("//*[@id='container-div']/img")).isDisplayed())
			driver.findElement(By.xpath("//*[@id='container-div']/img")).click();*/
		
		String parentID = driver.getWindowHandle();
		WebElement agriClick = driver.findElement(By.xpath("//a[@href='/htdocs/common/agri/index.html'][contains(text(), 'Agri')]"));
		agriClick.click();
		
		switchToChildwindow(parentID);
		switchToWindowByTitle("HDFC Bank Kisan Dhan Vikas e-Kendra");
		String getTitle = driver.getTitle();
		Assert.assertEquals("HDFC Bank Kisan Dhan Vikas e-Kendra", getTitle);
		
		CloseAllWindowOutparentWindow(parentID);
		
		
	}
	public void switchToChildwindow(String parent) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String runWindow: allWindows)
		{
		 if (runWindow.equals(parent))
		{
			driver.switchTo().window(runWindow);
		break;}
		}}
	
	public void switchToWindowByTitle(String expectedTitle) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String runWindow: allWindows) {
		driver.switchTo().window(runWindow);
String actualTittle = driver.getTitle();
if(actualTittle.equals(expectedTitle)){
		break;}
		}}
	

public boolean CloseAllWindowOutparentWindow(String parentID) {
	Set<String> allWindows = driver.getWindowHandles();
	for (String runWindow: allWindows)
	{
	 if (!runWindow.equals(parentID))
	{
		driver.switchTo().window(runWindow);
		driver.close();
	}
	}
		
		driver.switchTo().window(parentID);
		if (driver.getWindowHandles().size() == 1) 
			
		return true;
		else
		return false;
}

	@AfterClass
	public void afterClass() {

		driver.quit();
	}
}

