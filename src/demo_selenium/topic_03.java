package demo_selenium;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class topic_03 {
	WebDriver driver;
	@BeforeTest
	public void int_data()
	{
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		
		
	}

	@BeforeMethod
	public void beforeTestCase() {
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize(); 
		
		String homePageTittle = driver.getTitle();
		Assert.assertEquals("SELENIUM WEBDRIVER FORM DEMO", homePageTittle);
		
		
	}

	@Test
	public void TestCase01_CheckDisplay() {

		WebElement email =driver.findElement(By.xpath("//*[@name='user_email']"));
		email.sendKeys("Automation Testing ");
	
		WebElement age =driver.findElement(By.xpath("//*[@id='under_18']"));
		age.click();
		
		WebElement education = driver.findElement(By.xpath("//*[@name='user_edu']"));
		education.sendKeys("Automation Testing ");
		
	}
	
	@Test
	public void TestCase02_CheckEnableDisable() {

		CheckElement("//*[@name='user_email']");
		CheckElement("//*[@name='user_pass']");
		CheckElement("//*[@id='under_18']");
		CheckElement("//*[@name='user_edu']");
		CheckElement("//*[@name='user_bio']");
		CheckElement("//*[@name='user_job1']");
		CheckElement("//*[@name='user_job2']");
		CheckElement("//input[@id='development']");	
		CheckElement("//button[@id='button-disabled']");
	    CheckElement("//button[@id='button-enabled']");
		
}	
	public void CheckElement(String xpath)
		{
			WebElement elemment =driver.findElement(By.xpath(xpath));
			if(elemment.isEnabled())
			{
				System.out.println("elemment is enable");
			}
			else
			{
				System.out.println("elemment is Disable");
			}
		}
		
	
		@Test
		public void TestCase03_CheckClick() {
			
			boolean value_age = false;
			WebElement radio_age = driver.findElement(By.xpath("//*[@id='under_18']"));
			value_age = radio_age.isSelected();
			if (value_age==false) {
				radio_age.click();
				System.out.println("age under 18 is Selected");
			}
			else System.out.println("age under 18 is not Select");
				
			
			boolean value_Interests = false;
			WebElement checkbox_Interests = driver.findElement(By.xpath("//input[@id='development']"));
			value_Interests = checkbox_Interests.isSelected();
			if (value_Interests==false) {
				checkbox_Interests.click();
				System.out.println("Interests developer is Selected");
			}
			else System.out.println("Interests developer is not Select");
		
				
				TestCase02_CheckEnableDisable();
			
			
		}	
		
			 

			
		
	
	
	
	

	@AfterClass
	public void afterClass() {

		driver.quit();
	}
}
