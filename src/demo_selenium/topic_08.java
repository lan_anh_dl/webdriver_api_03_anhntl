package demo_selenium;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class topic_08 {
	WebDriver driver;
	@BeforeTest
	public void int_data()
	{
		/*System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		
		System.setProperty("webdriver.ie.driver", ".\\driver\\IEDriverServer.exe");
		driver = new InternetExplorerDriver();*/
		
		driver = new FirefoxDriver();
		
	}
	//Highlight an element:
		public void highlightElement( WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].style.border='6px groove red'", element);
		}
		//Execute for Browser
		public Object executeForBrowserElement( String javaSript) {
		try {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		return js.executeScript(javaSript);
		} catch (Exception e) {
		e.getMessage();
		return null;
		}
		}
		//Execute for WebElement
		public Object executeForWebElement( WebElement element) {
		try {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		return js.executeScript("arguments[0].click();", element);
		} catch (Exception e) {
		e.getMessage();
		return null;
		}
		}
		// Remove attribute in DOM
		public Object removeAttributeInDOM( WebElement element,
		String attribute) {
		try {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		return js.executeScript("arguments[0].removeAttribute('" + attribute +
		"');", element);
		} catch (Exception e) {
		e.getMessage();
		return null;
		}
		}
		
		public Object scrollToBottomPage() {
			try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
			} catch (Exception e) {
			e.getMessage();
			return null;
			}
			}
	public Object navigateDiffrentPage(String UrlOther)
	{
		JavascriptExecutor js = (JavascriptExecutor)driver;
		return js.executeScript("window.location = '"+UrlOther+"'");
	}

	@Test
	public void TestCase01_() {

		driver.get(" http://live.guru99.com/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize(); 
		
		String domainName = (String) executeForBrowserElement("return document.title");
		Assert.assertEquals("Home page", domainName);
	
		String URLPage = (String) executeForBrowserElement("return document.URL");
		Assert.assertEquals("http://live.guru99.com/", URLPage);
		
		WebElement mobilePage = driver.findElement(By.xpath("//a[contains(text(),'Mobile')]"));
		highlightElement(mobilePage);
		executeForWebElement(mobilePage);
		
		WebElement addProduct = driver.findElement(By.xpath("//h2[a[contains(text(),'Samsung Galaxy')]]/following-sibling::div[@class='actions']//button"));
		highlightElement(addProduct);
		executeForWebElement(addProduct);
		
		String addText = (String) executeForBrowserElement("return document.documentElement.innerText;");
		Assert.assertTrue(addText.contains("Samsung Galaxy was added to your shopping cart."));
		
		WebElement privacyPage = driver.findElement(By.xpath("//a[contains(text(),'Privacy Policy')]"));
		highlightElement(privacyPage);
		executeForWebElement(privacyPage);
		
		String titlePrivacyPage = (String) executeForBrowserElement("return document.title");
		Assert.assertEquals("Privacy Policy", titlePrivacyPage);
		scrollToBottomPage();
		
		WebElement wishList = driver.findElement(By
		.xpath("//th[contains(text(),'WISHLIST_CNT')]/following-sibling::td[contains(text(),'The number of items in your Wishlist.')]"));
		highlightElement(wishList);
		Assert.assertTrue(wishList.isDisplayed());
		
		navigateDiffrentPage("http://demo.guru99.com/v4/" );
		String domainNameDemoGuruPage = (String) executeForBrowserElement("return document.URL");
		Assert.assertEquals("http://demo.guru99.com/v4/", domainNameDemoGuruPage);
	}
	
	@Test
	
	public void TestCase02_() throws Exception {

		driver.get("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_input_disabled");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize(); 
		
		WebElement lastNameFrame =driver.findElement(By.xpath("//iframe[@id='iframeResult']"));
		driver.switchTo().frame(lastNameFrame);
		
		WebElement lastnametxt = driver.findElement(By.xpath("//input[@name='lname']"));
		removeAttributeInDOM(lastnametxt,"disabled");
	
		WebElement firstNametxt = driver.findElement(By.xpath("//input[@name='fname']"));
		String lastname ="Test Automation";
		String fristname ="Selenium";
		lastnametxt.sendKeys(lastname);
		firstNametxt.sendKeys(fristname);
		
		WebElement submitButton = driver.findElement(By.xpath("//input[@value='Submit']"));
		executeForWebElement(submitButton);
		
		Thread.sleep(3000);
		
		String innerText = (String) executeForBrowserElement("return document.documentElement.innerText;");
		Assert.assertTrue(innerText.contains(lastname));
		Assert.assertTrue(innerText.contains(fristname));
		
	}
	
	@AfterClass
	public void afterClass() {

		driver.quit();
	}
}
