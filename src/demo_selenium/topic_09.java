package demo_selenium;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class topic_09 {
	WebDriver driver;
	WebDriverWait wait;
	String filepath = "C:\\Users\\Lan Anh\\Desktop\\hoadep.png";
	String name= "Hoi lam gi";
	String fileName ="hoadep.png";
	String folder = "UploaAutomation new"+ randomNumber();
	String email="kakaneme"+ randomNumber()+"@gmail.com";
	@BeforeTest
	public void int_data()
	{
		/*System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		driver = new ChromeDriver();*/
		
		
		driver = new FirefoxDriver();
		
		/*System.setProperty("webdriver.ie.driver", ".\\driver\\IEDriverServer.exe");
		  DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
	    // this line of code is to resolve protected mode issue capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
	    capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

			driver = new InternetExplorerDriver();*/
	}
	public Boolean checkAnyImagLoaded(WebElement image )
	{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		return (Boolean) js.executeScript("return arguments[0].complete &&"+" typeof arguments[0].naturalWidth != 'undefined' && arguments[0].naturalWidth > 0", image);
	}

	
	public Object executeForWebElement( WebElement element) {
		try {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		return js.executeScript("arguments[0].click();", element);
		} catch (Exception e) {
		e.getMessage();
		return null;
		}
		}
	@Test
	public void uploadBysenkeyTestCase01() throws Exception {

		
			driver.get("http://blueimp.github.io/jQuery-File-Upload/");
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.manage().window().maximize(); 
			
			Thread.sleep(3000);
			WebElement addfileButton = driver.findElement(By.xpath("//input[@type='file']"));
			addfileButton.sendKeys(filepath);
			
			Thread.sleep(5000);
			
			WebElement checkAffterUpload = driver.findElement(By.xpath("//p[@class='name' and contains(text(),'"+ fileName + "')]"));
			Assert.assertTrue(checkAffterUpload.isDisplayed());
			
			WebElement StartButton = driver.findElement(By.xpath("//span[text()='Start']"));
			StartButton.click();
			Thread.sleep(5000);
			
			WebElement imageUploaded = driver.findElement(By.xpath("//img[contains(@src,'"+ fileName +"')]"));
			Assert.assertTrue(checkAnyImagLoaded(imageUploaded));
		
	}

	@Test
	public void UploadByAutoITTestCase02() {

		
		
	}
	@Test
	public void UploadRobotByTestCase03_() throws Exception {
		driver.get("http://blueimp.github.io/jQuery-File-Upload/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		//Specify the file location with extension
		StringSelection select = new StringSelection(filepath);
		//Copy to clipboard
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(select, null);
		//Click
		WebElement addfileButton = driver.findElement(By.xpath("//input[@type='file']"));
		executeForWebElement(addfileButton);
		
		Robot robot = new Robot();
		Thread.sleep(1000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);
		Thread.sleep(1000); 
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		
		WebElement checkAffterUpload = driver.findElement(By.xpath("//p[@class='name' and contains(text(),'"+ fileName + "')]"));
		Assert.assertTrue(checkAffterUpload.isDisplayed());
		
		WebElement StartButton = driver.findElement(By.xpath("//span[text()='Start']"));
		executeForWebElement(StartButton);
		Thread.sleep(5000);
		
		WebElement imageUploaded = driver.findElement(By.xpath("//img[contains(@src,'"+ fileName +"')]"));
		Assert.assertTrue(checkAnyImagLoaded(imageUploaded));
	}
	@Test
	public void UploadTestCase04_() throws Exception {
		driver.get("https://encodable.com/uploaddemo/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		WebElement addfileButton = driver.findElement(By.xpath("//input[@id='uploadname1']"));
		addfileButton.sendKeys(filepath);
		
		WebElement newFolder = driver.findElement(By.xpath("//*[@id='newsubdir1']"));
		newFolder.sendKeys(folder);
		
		WebElement newEmail = driver.findElement(By.xpath("//*[@id='formfield-email_address']"));
		newEmail.sendKeys(email);
		
		WebElement newName = driver.findElement(By.xpath("//*[@id='formfield-first_name']"));
		newName.sendKeys(name);
		
		WebElement uploadButton = driver.findElement(By.xpath("//*[@id='uploadbutton']"));
		executeForWebElement(uploadButton);
		
		Thread.sleep(10000);
		
		WebElement verifyEmail = driver.findElement(By.xpath("//dd[contains(text(),'Email Address: "+email+"')]"));
		Assert.assertTrue(verifyEmail.isDisplayed());
		
		WebElement verifyName = driver.findElement(By.xpath("//dd[contains(text(),'First Name: "+name+"')]"));
		Assert.assertTrue(verifyName.isDisplayed());
		
		WebElement verifyFile = driver.findElement(By.xpath("//a[contains(text(),'"+fileName+"')]"));
		Assert.assertTrue(verifyFile.isDisplayed());
		
		WebElement linkViewUpload = driver.findElement(By.xpath("//a[contains(text(),'View Uploaded Files')]"));
		executeForWebElement(linkViewUpload);
		
		WebElement linkfolder = driver.findElement(By.xpath("//a[contains(text(),'"+folder+"')]"));
		executeForWebElement(linkfolder);
		
		WebElement resurt = driver.findElement(By.xpath("//img[contains(@src, '"+fileName+"')]"));
		Assert.assertTrue(resurt.isDisplayed());
		
		
		
		
		
	}
	
	
	public int randomNumber()
	
	{
		Random random = new Random();
		int rdNumber = random.nextInt(1000);
		return rdNumber;
	}
	
	@AfterClass
	public void afterClass() {

		driver.quit();
	}
}
