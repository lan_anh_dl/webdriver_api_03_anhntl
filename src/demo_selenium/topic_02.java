package demo_selenium;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class topic_02 {
	
	WebDriver driver;
	@BeforeTest
	public void int_data()
	{
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		
		
	}

	@BeforeMethod
	public void beforeTestCase() {
		driver.get("http://live.guru99.com/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize(); 
		
		String homePageTittle = driver.getTitle();
		Assert.assertEquals("Home page", homePageTittle);
		
		WebElement myAccountLink = driver.
				findElement(By.xpath("//div[@class='footer-container']//a[@title='My Account']"));
		myAccountLink.click();
		
	}

	@Test(priority = 0)
	public void TestCase1_VerifyURL () {
		WebElement createAcountLink = driver
				.findElement(By.xpath("//a[@title='Create an Account']"));		
		createAcountLink.click();
		
		driver.navigate().back();
		String loginPageUrl = driver.getCurrentUrl();
		Assert.assertEquals("http://live.guru99.com/index.php/customer/account/login/", loginPageUrl);
		driver.navigate().forward();
		String CreateAcountPageUrl = driver.getCurrentUrl();
		Assert.assertEquals("http://live.guru99.com/index.php/customer/account/create/", CreateAcountPageUrl);
	}
	@Test(priority = 1)
		public void TestCase2_loginEmpty() {
		
		//login click
				WebElement buttonLogin =driver.findElement(By.xpath("//*[@title='Login']"));
				buttonLogin.click();
				WebElement messeEmailFail = driver.findElement(By.xpath("//div[@id='advice-required-entry-email']"));
				Assert.assertEquals("This is a required field.", messeEmailFail.getText());
				
				WebElement messePassError =driver.findElement(By.xpath("//div[@id='advice-required-entry-pass']"));
				Assert.assertEquals("This is a required field.", messePassError.getText());
				
		}
	@Test(priority = 2)
		public void TestCase03_emailInvalid() {
			
		WebElement EmailInvaid = driver.findElement(By.xpath("//*[@title='Email Address']"));
		EmailInvaid.sendKeys("123434234@12312.123123");
		
		WebElement buttonLogin =driver.findElement(By.xpath("//*[@title='Login']"));
		buttonLogin.click();
		
		WebElement messEmailInvai = driver.findElement(By.xpath("//div[@id='advice-validate-email-email']"));
		Assert.assertEquals("Please enter a valid email address. For example johndoe@domain.com.", messEmailInvai.getText());
	}
	
	@Test(priority = 3)
	public void TestCase04_Passwordincorrect() {
		
	WebElement Emailcorrec = driver.findElement(By.xpath("//*[@title='Email Address']"));
	Emailcorrec.sendKeys("automation@gmail.com");
	
	WebElement PassInvaid = driver.findElement(By.xpath("//*[@title='Password']"));
	PassInvaid.sendKeys("123");
	
	WebElement buttonLogin =driver.findElement(By.xpath("//*[@title='Login']"));
	buttonLogin.click();
	
	WebElement messEmailInvai = driver.findElement(By.xpath("//div[@id='advice-validate-password-pass']"));
	Assert.assertEquals("Please enter 6 or more characters without leading or trailing spaces.", messEmailInvai.getText());
}
	
	@Test(priority = 4)
	public void TestCase05_CreateAnAccount() {
		
	
	WebElement buttonCreateacount =driver.findElement(By.xpath("//*[@title='Create an Account']"));
	buttonCreateacount.click();
	
	WebElement FistName = driver.findElement(By.xpath("//*[@title='First Name']"));
	FistName.sendKeys("Duong");
	
	WebElement MiddName = driver.findElement(By.xpath("//*[@name='middlename']"));
	MiddName.sendKeys("thị");
	
	WebElement LasName = driver.findElement(By.xpath("//*[@name='lastname']"));
	LasName.sendKeys("nguyễn");
	
	
	WebElement Email = driver.findElement(By.xpath("//*[@title='Email Address']"));
	
	Random generate = new Random();
	  
	Email.sendKeys("duongthi" + generate.nextInt(100) +"@gmail.com");
	
	
	
	WebElement pass = driver.findElement(By.xpath("//*[@title='Password']"));
	pass.sendKeys("abc!123");
	
	WebElement ConfPass = driver.findElement(By.xpath("//*[@title='Confirm Password']"));
	ConfPass.sendKeys("abc!123");
	
	WebElement Upfornewlester = driver.findElement(By.xpath("//*[@title='Sign Up for Newsletter']"));
	Upfornewlester.click();
	Assert.assertTrue(Upfornewlester.isSelected());
	
	WebElement buttonRegister =driver.findElement(By.xpath("//*[@id='form-validate']/div[2]/button"));
	buttonRegister.click();
	
	WebElement sussesmess = driver.findElement(By.cssSelector(".success-msg>ul>li>span"));
	Assert.assertEquals("Thank you for registering with Main Website Store.", sussesmess.getText());
	
	WebElement howeracount =driver.findElement(By.xpath("//*[@id='header']/div/div[2]/div/a/span[2]"));
	howeracount.click();
	WebElement lockoutLink =driver.findElement(By.xpath("//a[@title='Log Out']"));
	lockoutLink.click();
	
	
}
	
	
		
	@AfterClass
	public void afterTestCase() {
		
		driver.quit();
	}
}
