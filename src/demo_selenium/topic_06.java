package demo_selenium;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class topic_06 {
	WebDriver driver;
	@BeforeTest
	public void int_data()
	{
		/*System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		driver = new ChromeDriver();*/
		driver = new FirefoxDriver();
		
		
	}


	@Test
	public void TestCase01_Hover() throws Exception {
		
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize(); 
		
		String homePageTittle = driver.getTitle();
		Assert.assertEquals("SELENIUM WEBDRIVER FORM DEMO", homePageTittle);
		
		WebElement hoverMouse = driver.findElement(By.xpath("//a[contains(text(), 'Hover over me')]"));
		
		Actions action = new Actions(driver);
		action.moveToElement(hoverMouse).perform();
		Thread.sleep(3000);
		
		WebElement hoorayText = driver.findElement(By.xpath("//div[@class='tooltip-inner' and contains(text(), 'Hooray!')]"));
		Assert.assertTrue(hoorayText.isDisplayed());
	
	}
	

	@Test
	public void Testcase_02_LoginHover() throws Exception {
		
		driver.get("http://www.myntra.com/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize(); 
		
		
		WebElement hoverLogin = driver.findElement(By.xpath("//div[@class='desktop-userIconsContainer']"));
		Actions action = new Actions(driver);
		action.moveToElement(hoverLogin).perform();
		
		
		WebElement loginButton = driver.findElement(By.xpath("//a[@class='desktop-linkButton' and contains(text(), 'login')]"));
		loginButton.click();
		
		WebElement LoginButton= driver.findElement(By.xpath("//*[@id='mountRoot']//div[@class='login-box']"));
		Assert.assertTrue(LoginButton.isDisplayed());
	
	}
	
	@Test
	public void Testcase_03_ClickHold() throws Exception {
		
		driver.get("http://jqueryui.com/resources/demos/selectable/display-grid.html");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize(); 
		
		List <WebElement> listItems = driver.findElements(By.xpath("//*[@id='selectable']/li"));
		Actions builder = new Actions(driver);
		builder.clickAndHold(listItems.get(0)).clickAndHold(listItems.get(3)).click().perform();
		
		List <WebElement> listSelect = driver.findElements(By.xpath("//*[@id='selectable']/li[contains(@class, 'ui-selected')]"));
		int number =listSelect.size();
		Assert.assertEquals(4, number);
	
	}
	
	@Test
	public void Testcase_04_RightClick() throws Exception {
		
		driver.get("http://swisnl.github.io/jQuery-contextMenu/demo.html");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize(); 
		
		WebElement buttonRightClick  = driver.findElement(By.xpath("//span[text()='right click me']"));
		Actions action = new Actions(driver);
		action.contextClick(buttonRightClick).perform();
		
		WebElement quitDisplay = driver.findElement(By.xpath("//li[contains(@class,'context-menu-icon-quit') and contains(.,'Quit')]"));
		action.moveToElement(quitDisplay).perform();
		//Assert.assertTrue(quitDisplay.isDisplayed());
		
		WebElement quitVisble = driver.findElement(By.xpath("//li[contains(@class,'context-menu-visible') and contains(.,'Quit')]"));
		//action.moveToElement(quitVisble);
		Assert.assertTrue(quitVisble.isDisplayed());
		
		WebElement quitHover = driver.findElement(By.xpath("//li[contains(@class,'context-menu-hover') and contains(.,'Quit')]"));
		//action.moveToElement(quitHover);
		Assert.assertTrue(quitHover.isDisplayed());
		quitHover.click();
	
	}
	
	@Test
	public void Testcase_05_DragDropt() throws Exception {
		
		driver.get("http://demos.telerik.com/kendo-ui/dragdrop/angular");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize(); 
		
		WebElement CỉrcleBig  = driver.findElement(By.xpath("//*[@id='draggable']"));
		
		WebElement CỉrcleSmall  = driver.findElement(By.xpath("//*[@id='droptarget']"));
		Actions action = new Actions(driver);
		
		action.dragAndDrop(CỉrcleBig, CỉrcleSmall).build().perform();
		//Thread.sleep(3000);
		Assert.assertTrue(driver.findElement(By.xpath("//*[@id='droptarget']")).getText().contains("You did great!"));
		
		
	
	}
	
	
	@Test
	public void Testcase_05_DragDropt_2() throws Exception {
		
		driver.get("http://jqueryui.com/resources/demos/droppable/default.html");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize(); 
		
		WebElement Big  = driver.findElement(By.xpath("//*[@id='draggable']"));
		
		WebElement Small  = driver.findElement(By.xpath("//*[@id='droppable']"));
		Actions action = new Actions(driver);
		
		action.dragAndDrop(Big, Small).build().perform();
		//Thread.sleep(3000);
		Assert.assertTrue(driver.findElement(By.xpath("//*[@id='droppable']")).getText().contains("Dropped!"));
		
		
	
	}


	
	
	
	
	
	
	@AfterClass
	public void afterClass() {

		driver.quit();
	}
}
