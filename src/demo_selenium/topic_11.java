package demo_selenium;

import org.testng.annotations.Test;

import com.google.common.base.Function;

//import com.thoughtworks.selenium.Wait;


import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class topic_11 {
	WebDriver driver;
	WebDriverWait wait;
	FluentWait fluentWait;
	@BeforeTest
	public void int_data()
	{
		System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		 wait = new WebDriverWait(driver, 30);
		
	}

	
	
	@Test
	public void TestCase01_Implicitly() {

		
		driver.get("http://the-internet.herokuapp.com/dynamic_loading/2");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize(); 
		
		WebElement StartButton = driver.findElement(By.xpath("//*[@id='start']/button"));
		StartButton.click();
		
		
		WebElement textHello = driver.findElement(By.xpath("//*[@id='finish']/h4"));
		Assert.assertEquals("Hello World!", textHello.getText());
	}
	@Test
public void TestCase02_explicipt() {

		
		driver.get("http://demos.telerik.com/aspnet-ajax/ajaxloadingpanel/functionality/explicit-show-hide/defaultcs.aspx");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize(); 
		
		WebElement daytimePiker = driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceholder1_Panel1']/div"));
		Assert.assertTrue(daytimePiker.isDisplayed());
		
		
		WebElement daySelected = driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceholder1_ctl00_ContentPlaceholder1_Label1Panel']/span"));
		System.out.print("ngay truoc khi chon : "+ daySelected.getText());
		Assert.assertEquals("No Selected Dates to display.", daySelected.getText());
		
		WebElement TodaySelected = driver.findElement(By.xpath("//a[text()='21']"));
		TodaySelected.click();
		
		By waitAjaxIcon = By.xpath("//div[@class='raDiv']");
		wait.until(ExpectedConditions.invisibilityOfElementLocated(waitAjaxIcon));
		
		
		WebElement daySelectedAfter = driver.findElement(By.xpath("//*[@id='ctl00_ContentPlaceholder1_ctl00_ContentPlaceholder1_Label1Panel']/span"));
		System.out.print("ngay sau khi chon : "+ daySelectedAfter.getText());
		Assert.assertEquals("Monday, May 21, 2018", daySelectedAfter.getText());
			
	}
	
	
	@Test
	public void TestCase03_FluentWait() {

			
			driver.get("https://stuntcoders.com/snippets/javascript-countdown/");
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.manage().window().maximize(); 
			
			WebElement countdown = driver.findElement(By.xpath("//*[@id='javascript_countdown_time']"));
			Assert.assertTrue(countdown.isDisplayed());
			
			
			By countdownTime = By.xpath("//*[@id='javascript_countdown_time']");
			wait.until(ExpectedConditions.visibilityOfElementLocated(countdownTime));
			
			new FluentWait<WebElement>(countdown)
			.withTimeout(10, TimeUnit.SECONDS)
			.pollingEvery(100, TimeUnit.MILLISECONDS)
			.ignoring(NoSuchElementException.class)
			.until(new Function<WebElement, Boolean>()
			{public Boolean apply(WebElement element)
			{boolean flag = element.getText().endsWith("00");
			System.out.println(element.getText());
			return flag;
			}
			}
			);

		}
	@Test
	public void TestCase03_FluentWait2() {

			
			driver.get("http://toolsqa.wpengine.com/automation-practice-switch-windows/");
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.manage().window().maximize(); 
			
			WebElement changeColor = driver.findElement(By.xpath("//*[@id='colorVar']"));
			Assert.assertTrue(changeColor.isDisplayed());
			
			new FluentWait<WebElement>(changeColor)
			.withTimeout(10, TimeUnit.SECONDS)
			.pollingEvery(5, TimeUnit.SECONDS)
			.ignoring(NoSuchElementException.class)
			.until(new Function<WebElement, Boolean>()
			{public Boolean apply(WebElement element)
			{boolean flag = element.getAttribute("style").equals("color: red;");
			//System.out.println(element.getText());
			return flag;
			}
			}
			);
			
			WebElement buzz = driver.findElement(By.xpath("//*[@id='clock']"));
			Assert.assertTrue(buzz.isDisplayed());
			
			new FluentWait<WebElement>(buzz)
			.withTimeout(40, TimeUnit.SECONDS)
			.pollingEvery(100, TimeUnit.MILLISECONDS)
			.ignoring(NoSuchElementException.class)
			.until(new Function<WebElement, Boolean>()
			{public Boolean apply(WebElement element)
			{boolean flag = element.getText().matches("Buzz Buzz");
			//System.out.println(element.getText());
			return flag;
			}
			}
			);
			
			
				
		} 
		
	
	
	@AfterClass
	public void afterClass() {

		driver.quit();
	}
}
