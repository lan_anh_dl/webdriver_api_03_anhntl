package Selenium_TestNG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;

public class tescase02Group {
	WebDriver driver;
	  @Test (groups = {"google"})
	  public void tc_01() {
		  
		  driver.get("https://Google.com");
		  String homePageTittle = driver.getTitle();
			Assert.assertEquals("Google", homePageTittle);
			System.out.println(homePageTittle);
	  }
	  
	  @Test (groups = {"google"})
	  public void Tc_02() {
		  
		  String url = driver.getCurrentUrl();
		  System.out.println(url);
	  }
	  
	  @Test (groups = {"facebook"})
	  public void tc_03() {
		  
		  driver.get("https://facebook.com");
		  String homePageTittle = driver.getTitle();
			//Assert.assertEquals("Google", homePageTittle);
			System.out.println(homePageTittle);
	  }
	  
	  @Test (groups = {"facebook"})
	  public void Tc_04() {
		  
		  String url = driver.getCurrentUrl();
		  System.out.println(url);
	  }

	 
	  @BeforeClass(alwaysRun = true)
	  public void beforeClass() {
		  System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
			driver = new ChromeDriver();
	  }

	  @AfterClass
	  public void afterClass() {
		  System.out.println("afterClass");
		  driver.quit();
	  }

}
