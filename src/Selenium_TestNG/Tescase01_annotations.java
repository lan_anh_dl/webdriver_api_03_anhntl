package Selenium_TestNG;


import org.testng.annotations.Test;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class Tescase01_annotations {
	WebDriver driver;
  @Test
  public void tc_01() {
	  
	  driver.get("https://Google.com");
	  String homePageTittle = driver.getTitle();
		Assert.assertEquals("Google", homePageTittle);
		System.out.println(homePageTittle);
  }
  
  @Test
  public void Tc_02() {
	  
	  String url = driver.getCurrentUrl();
	  System.out.println(url);
  }
  


 
  @BeforeClass
  public void beforeClass() {
	  
	  System.out.println("beforeClass");
	  System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
  }

  @AfterClass
  public void afterClass() {
	  System.out.println("afterClass");
	  driver.quit();
  }

  @BeforeTest
  public void beforeTest() {
	  System.out.println("beforeTest");
  }

  @AfterTest
  public void afterTest() {
	  System.out.println("afterTest");
  }

  @BeforeSuite
  public void beforeSuite() {
	  System.out.println("beforeSuite");
  }

  @AfterSuite
  public void afterSuite() {
	  System.out.println("afterSuite");
  }

}
