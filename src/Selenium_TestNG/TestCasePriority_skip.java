package Selenium_TestNG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class TestCasePriority_skip {
	@Test(priority = 5, enabled= true)
	  public void tc_01() {
		  
		 
			System.out.println("TC 01");
	  }
	  
	  @Test (priority = 1)
	  public void Tc_02() {

			System.out.println("TC 02");
	  }
	  
	  @Test(priority = 5, enabled= false)
	  public void tc_03() {
		  
		 
			System.out.println("TC 03");
	  }
	  
	  @Test(priority = 4, enabled= true)
	  public void Tc_04() {

			System.out.println("TC 04");
	  }
	  
	  @Test (priority = 3, enabled= false)
	  public void tc_05() {
		  
		 
			System.out.println("TC 05");
	  }
	  
	  @Test (priority = 2)
	  public void Tc_06() {

			System.out.println("TC 06");
	  }
	  
}
