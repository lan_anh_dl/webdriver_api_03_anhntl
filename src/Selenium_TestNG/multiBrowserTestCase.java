package Selenium_TestNG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class multiBrowserTestCase {
	public WebDriver driver;
	  @Test
	  public void tc_01() {
		  
		  driver.get("https://Google.com");
		  String homePageTittle = driver.getTitle();
			Assert.assertEquals("Google", homePageTittle);
		
	  }
	  
	  @Parameters({"browser"})
@BeforeTest
public void beforeTest(String browser) {
		  if(browser.equals("chrome"))
		  {
		  
	  System.setProperty("webdriver.chrome.driver", ".\\driver\\chromedriver.exe");
		driver = new ChromeDriver();
		  }
		  if(browser.equals("firefox"))
			  
		  {
			  System.setProperty("webdriver.geckodriver.driver", ".\\driver\\geckodriver.exe");
	  driver = new FirefoxDriver();
		  }
	  
		  if(browser.equals("ie"))
		  {
	  System.setProperty("webdriver.ie.driver", ".\\driver\\IEDriverServer.exe");
	  DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
    // this line of code is to resolve protected mode issue capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
    capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

		driver = new InternetExplorerDriver();
		  }
		  }

@AfterTest
public void afterTest() {
	  driver.quit();
}

}
